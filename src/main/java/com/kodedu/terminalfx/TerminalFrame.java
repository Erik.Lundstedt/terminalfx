package com.kodedu.terminalfx;

import com.kodedu.terminalfx.config.Config;
import com.kodedu.terminalfx.config.TerminalConfig;
import com.kodedu.terminalfx.helper.IOHelper;
import com.kodedu.terminalfx.helper.ThreadHelper;
import com.pty4j.PtyProcess;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.TitledPane;

import java.io.Reader;
import java.io.Writer;
import java.nio.file.Path;
import java.util.Objects;


public class TerminalFrame extends TitledPane
{
	Config cfg=new Config();


	private final Terminal term;

	public TerminalFrame(TerminalConfig terminalConfig, Path terminalPath) {
		this(new Terminal(terminalConfig, terminalPath));
	}

	public TerminalFrame(Terminal terminal) {
		this.term = terminal;
		setContent(term);

		System.out.println(term.getTerminalPath());

	}




	public void closeTerminal(ActionEvent... actionEvent) {
				Platform.exit();
				destroy();
				System.exit(0);
		}


	public void destroy() {
		ThreadHelper.start(() -> {
			while (Objects.isNull(getProcess())) {
				ThreadHelper.sleep(250);
			}
			getProcess().destroy();
			IOHelper.close(getInputReader(), getErrorReader(), getOutputWriter());
		});
	}








	public void onTerminalFxReady(Runnable onReadyAction) {
		term.onTerminalFxReady(onReadyAction);
	}

	public Path getTerminalPath() {
		return term.getTerminalPath();
	}

	public TerminalConfig getTerminalConfig() {
		return term.getTerminalConfig();
	}

	public PtyProcess getProcess() {
		return term.getProcess();
	}

	public Reader getInputReader() {
		return term.getInputReader();
	}

	public Reader getErrorReader() {
		return term.getErrorReader();
	}

	public Writer getOutputWriter() {
		return term.getOutputWriter();
	}

	public Terminal getTerm() {
		return term;
	}

}
