package com.kodedu.terminalfx;

import com.kodedu.terminalfx.config.Config;
import com.kodedu.terminalfx.config.TerminalConfig;
import com.kodedu.terminalfx.helper.RGB;
import javafx.fxml.Initializable;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.net.URL;
import java.util.ResourceBundle;



public class FXMLController implements Initializable {


	Config cfg=new Config();
	//public TitledPane termPane;
	public AnchorPane 	anchorPane;

	public BorderPane 	termPane;

	public TextField 	leftStatus;
	public TextField 	centerStatus;
	public TextField 	rightStatus;
	public HBox 		horizontalBox;




	@Override
	public void initialize(URL url, ResourceBundle rb) {

		TextField[] textFields={leftStatus,centerStatus,rightStatus};
/*
//        default Dark Config
        TerminalConfig darkConfig = new TerminalConfig();
        darkConfig.setBackgroundColor(Color.rgb(16, 16, 16));
        darkConfig.setForegroundColor(Color.rgb(240, 240, 240));
        darkConfig.setCursorColor(Color.rgb(255, 0, 0, 0.5));
//        CygWin Config
        TerminalConfig cygwinConfig = new TerminalConfig();
        cygwinConfig.setWindowsTerminalStarter("C:\\cygwin64\\bin\\bash -i");
        cygwinConfig.setFontSize(14);
//        Default Config
        TerminalConfig defaultConfig = new TerminalConfig();
*/
		TerminalConfig userConfig = new TerminalConfig();
		RGB bg=cfg.backgroundColor;
		RGB fg=cfg.foregroundColor;
		RGB cc=cfg.cursorColor;
		userConfig.setBackgroundColor(Color.rgb(bg.red,bg.green,bg.blue));
		userConfig.setForegroundColor(Color.rgb(fg.red,fg.green,fg.blue));
		userConfig.setCursorColor(Color.rgb(cc.red,cc.green,cc.blue, 0.5));
		userConfig.setCtrlVPaste(cfg.ctrlVPaste);
		userConfig.setScrollbarVisible(cfg.showScrollBar);
		userConfig.setUseDefaultWindowCopy(cfg.doDefaultWindowCopy);
		userConfig.setUnixTerminalStarter(cfg.shell);
		userConfig.setCopyOnSelect(cfg.coppyOnSelect);
		horizontalBox.alignmentProperty().set(Pos.BOTTOM_CENTER);

//		anchorPane.setOpacity(cfg.BGopacity);


		for (int i = 0; i < textFields.length; i++) {

			textFields[i].setText(cfg.getFooterText()[i]);
			textFields[i].setFont(Font.font("UbuntuMono NF"));
			textFields[i].setEditable(false);
			textFields[i].setOpacity(cfg.barOpacity);

			switch (i)
			{
				case 0:
					textFields[i].alignmentProperty().set(Pos.BOTTOM_LEFT);
					break;
				case 1:
					textFields[i].alignmentProperty().set(Pos.BOTTOM_CENTER);
					break;
				case 2:
					textFields[i].setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
					textFields[i].alignmentProperty().set(Pos.BOTTOM_RIGHT);
					break;
			}
		}

		TerminalBuilder terminalBuilder = new TerminalBuilder(userConfig);
		Terminal terminal = terminalBuilder.newTerminal();
		terminal.setOpacity(cfg.TermOpacity);
		termPane.setOpacity(cfg.BGopacity);
		termPane.setCenter(terminal);


	}


}
