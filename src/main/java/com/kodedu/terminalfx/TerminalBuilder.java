package com.kodedu.terminalfx;

import com.kodedu.terminalfx.config.TerminalConfig;

import java.nio.file.Path;
import java.util.Objects;

/**
 * Created by usta on 12.09.2016.
 */
public class TerminalBuilder {
	
	private Path terminalPath;
	private TerminalConfig terminalConfig;

	
	public TerminalBuilder() {
	}
	
	public TerminalBuilder(TerminalConfig terminalConfig) {
		this.terminalConfig = terminalConfig;
	}


	public TerminalConfig getTerminalConfig() {
		if (Objects.isNull(terminalConfig)) {
			terminalConfig = new TerminalConfig();
		}
		return terminalConfig;
	}
	
	public void setTerminalConfig(TerminalConfig terminalConfig) {
		this.terminalConfig = terminalConfig;
	}

	public Path getTerminalPath() {
		return terminalPath;
	}
	
	public void setTerminalPath(Path terminalPath) {
		this.terminalPath = terminalPath;
	}
	
	
	
	public Terminal newTerminal() {
		Terminal terminal = new Terminal(getTerminalConfig(), getTerminalPath());
		return terminal;
	}
}
