package com.kodedu.terminalfx.helper;

public class RGB {
	public int red;
	public int green;
	public int blue;

	public RGB(int red, int green, int blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}
	public RGB(int rgb[]) {
		this(rgb[0],rgb[1],rgb[2]);
	}

public String toCssStr()
{
	String ret="rgb(";
	ret+=String.valueOf(red);
	ret+=",";
	ret+=String.valueOf(green);
	ret+=",";
	ret+=String.valueOf(blue);
	ret+=")";
return ret;
}


}
