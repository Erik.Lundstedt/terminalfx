package com.kodedu.terminalfx.config;

import com.kodedu.terminalfx.helper.RGB;
import it.lundstedt.erik.StringMaker;
import javafx.scene.effect.Effect;

public class Config
{
	public String windowTitle="javaTerm";

	public String shell="/home/erik/scripts/abducoMenu";
	//public String shell="/home/erik/scripts/tmuxfzf";
	double uuid= Math.random()*100;
	
	
	//public String shell="/usr/local/bin/abduco -A javaTerm ";
	
	
	
	
	
	public RGB backgroundColor=Theme.background;
	public RGB foregroundColor=Theme.foreground;
	public RGB cursorColor=Theme.yellow;

	public RGB lightBackgroundColor=Theme.matteGreen;

	RGB bg=backgroundColor;
	RGB fg=foregroundColor;
	RGB cc=cursorColor;

	public float BGopacity=1.0f;
	public float TermOpacity=1.0f;
	public float barOpacity=1.0f;





	public boolean coppyOnSelect=true;


		public String[] getFooterText()
		{
			String[] retVal={"","",""};
			String javaIcon="\uE256 ";

			String os=System.getProperty("os.name");
			String osInf=System.getenv("HOME");


			String leftArrow="\uE0B0\uE0B1";
			String rightArrow="\uE0B3\uE0B2";


			String osSym="";
			if (os.equals("Linux"))
			{
				osSym=" \uF17C ";
			}

			String message,info,spacer = "";

			message="\tpress C+b d to detach from session\t";
			info="";
			for (int i = 0; i < 15; i++) {
				spacer+="\t";
			}

			//System.out.println("spacer"+spacer+"end");

			retVal[0]=leftArrow;
			String[] statArray={osInf,javaIcon,message,info,osSym};
			retVal[2]=rightArrow;
			StringMaker statusText=new StringMaker();
			statusText.setStrArray(statArray);

			retVal[1]=statusText.getStr();

			return retVal;

		}











	public boolean showScrollBar=false;
	public boolean ctrlVPaste=false;


	public boolean doDefaultWindowCopy=false;

	public Effect[] getEffect() {
		Effect e0;

		Effect[] effects={null,null,null};
//		effects[1];
		return effects;
	}
}





