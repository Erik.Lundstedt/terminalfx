package com.kodedu.terminalfx.config;

import com.kodedu.terminalfx.helper.FxHelper;
import javafx.scene.paint.Color;

import java.util.Objects;

public class TestConfig extends TerminalConfig
{

	private boolean useDefaultWindowCopy = true;


	private boolean clearSelectionAfterCopy = true;

	private boolean copyOnSelect = false;

	private boolean ctrlCCopy = true;

	private boolean ctrlVPaste = true;

	private String cursorColor = "black";

	private String backgroundColor = "white";

	private int fontSize = 14;

	private String foregroundColor = "black";

	private boolean cursorBlink = false;

	private boolean scrollbarVisible = true;

	private boolean enableClipboardNotice = true;

	private double scrollWhellMoveMultiplier = 0.1;

	private String fontFamily = "\"DejaVu Sans Mono\", \"Everson Mono\", FreeMono, \"Menlo\", \"Terminal\", monospace";

	private String userCss = "data:text/plain;base64," + "eC1zY3JlZW4geyBjdXJzb3I6IGF1dG87IH0=";

	private String windowsTerminalStarter = "cmd.exe";

	private String unixTerminalStarter = "/bin/bash -i";

	public boolean isUseDefaultWindowCopy() {
		return useDefaultWindowCopy;
	}

	public void setUseDefaultWindowCopy(boolean useDefaultWindowCopy) {
		this.useDefaultWindowCopy = useDefaultWindowCopy;
	}

	public boolean isClearSelectionAfterCopy() {
		return clearSelectionAfterCopy;
	}

	public void setClearSelectionAfterCopy(boolean clearSelectionAfterCopy) {
		this.clearSelectionAfterCopy = clearSelectionAfterCopy;
	}

	public boolean isCopyOnSelect() {
		return copyOnSelect;
	}

	public void setCopyOnSelect(boolean copyOnSelect) {
		this.copyOnSelect = copyOnSelect;
	}

	public boolean isCtrlCCopy() {
		return ctrlCCopy;
	}

	public void setCtrlCCopy(boolean ctrlCCopy) {
		this.ctrlCCopy = ctrlCCopy;
	}

	public boolean isCtrlVPaste() {
		return ctrlVPaste;
	}

	public void setCtrlVPaste(boolean ctrlVPaste) {
		this.ctrlVPaste = ctrlVPaste;
	}

	public String getCursorColor() {
		return cursorColor;
	}

	public void setCursorColor(String cursorColor) {
		this.cursorColor = cursorColor;
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public String getForegroundColor() {
		return foregroundColor;
	}

	public void setForegroundColor(String foregroundColor) {
		this.foregroundColor = foregroundColor;
	}

	public boolean isCursorBlink() {
		return cursorBlink;
	}

	public void setCursorBlink(boolean cursorBlink) {
		this.cursorBlink = cursorBlink;
	}

	public boolean isScrollbarVisible() {
		return scrollbarVisible;
	}

	public void setScrollbarVisible(boolean scrollbarVisible) {
		this.scrollbarVisible = scrollbarVisible;
	}

	public double getScrollWheelMoveMultiplier() {
		return scrollWhellMoveMultiplier;
	}

	public void setScrollWheelMoveMultiplier(double scrollWheelMoveMultiplier) {
		this.scrollWhellMoveMultiplier = scrollWheelMoveMultiplier;
	}

	public String getUserCss() {
		return userCss;
	}

	public void setUserCss(String userCss) {
		this.userCss = userCss;
	}

	public String getWindowsTerminalStarter() {
		return windowsTerminalStarter;
	}

	public void setWindowsTerminalStarter(String windowsTerminalStarter) {
		this.windowsTerminalStarter = windowsTerminalStarter;
	}

	public String getUnixTerminalStarter() {
		return unixTerminalStarter;
	}

	public void setUnixTerminalStarter(String unixTerminalStarter) {
		this.unixTerminalStarter = unixTerminalStarter;
	}

	public void setBackgroundColor(Color color) {
		setBackgroundColor(FxHelper.colorToHex(color));
	}

	public void setForegroundColor(Color color) {
		setForegroundColor(FxHelper.colorToHex(color));
	}

	public void setCursorColor(Color color) {
		setCursorColor(FxHelper.colorToHex(color));
	}

	public String getFontFamily() {
		return fontFamily;
	}

	public void setFontFamily(String fontFamily) {
		this.fontFamily = fontFamily;
	}

	public boolean isEnableClipboardNotice() {
		return enableClipboardNotice;
	}

	public void setEnableClipboardNotice(boolean enableClipboardNotice) {
		this.enableClipboardNotice = enableClipboardNotice;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		TestConfig that = (TestConfig) o;
		return useDefaultWindowCopy == that.useDefaultWindowCopy &&
				clearSelectionAfterCopy == that.clearSelectionAfterCopy &&
				copyOnSelect == that.copyOnSelect &&
				ctrlCCopy == that.ctrlCCopy &&
				ctrlVPaste == that.ctrlVPaste &&
				fontSize == that.fontSize &&
				cursorBlink == that.cursorBlink &&
				scrollbarVisible == that.scrollbarVisible &&
				enableClipboardNotice == that.enableClipboardNotice &&
				Double.compare(that.scrollWhellMoveMultiplier, scrollWhellMoveMultiplier) == 0 &&
				Objects.equals(cursorColor, that.cursorColor) &&
				Objects.equals(backgroundColor, that.backgroundColor) &&
				Objects.equals(foregroundColor, that.foregroundColor) &&
				Objects.equals(fontFamily, that.fontFamily) &&
				Objects.equals(userCss, that.userCss) &&
				Objects.equals(windowsTerminalStarter, that.windowsTerminalStarter) &&
				Objects.equals(unixTerminalStarter, that.unixTerminalStarter);
	}
}
