package com.kodedu.terminalfx.config;

import com.kodedu.terminalfx.helper.RGB;
import javafx.scene.paint.Color;

public class Theme {

	public static RGB colour0 = new RGB(35, 38, 53);// #232635
	public static RGB colour1 = new RGB(41, 45, 62);// #292d3e
	public static RGB colour2 = new RGB(60, 67, 94);// #3c435e

	public static RGB colour3 = new RGB(199, 146, 234);// #c792ea
	public static RGB colour4 = new RGB(173, 205, 129);// #adcd81
	public static RGB colour5 = new RGB(255, 203, 107);// #ffcb6b

	public static RGB colour6 = new RGB(105, 112, 152);// #697098
	public static RGB colour7 = new RGB(113, 124, 180);// #717cb4

	public static RGB colour8 = new RGB(150, 60, 89);// #963c59
	public static RGB colour9 = new RGB(67, 130, 123);// #43827b

	public static RGB background = colour0;
	public static RGB foreground = colour5;
	public static RGB text = colour5;
	public static RGB pink = colour3;
	public static RGB green = colour4;
	public static RGB matteGreen = colour9;

	public static RGB yellow = colour5;
	public static RGB red = colour8;


	public Theme() {
	Color test=new Color(10,20,30,0);
		System.out.println(test.toString());
	}
}