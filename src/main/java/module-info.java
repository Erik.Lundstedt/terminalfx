open module com.kodedu.terminalfx {

    /*
     */
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.web;
    requires org.apache.commons.lang3;
    requires com.fasterxml.jackson.annotation;
    requires pty4j;

    requires com.fasterxml.jackson.databind;
    requires jdk.jsobject;
	requires java.desktop;

	exports com.kodedu.terminalfx;
    exports com.kodedu.terminalfx.config;
    exports com.kodedu.terminalfx.helper;

}