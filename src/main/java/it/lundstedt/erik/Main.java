package it.lundstedt.erik;

import com.kodedu.terminalfx.TerminalAppStarter;

import java.io.IOException;

public class Main
{
	public static void main(String[] args) throws IOException {
				ProcessBuilder builder = new ProcessBuilder();
				double uuid = Math.random() * 100;
				builder.command("tmux","new-session","sysMenu");
				Process p= builder.start();
		TerminalAppStarter.main(args);
	}
}
