package it.lundstedt.erik;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class PopupMenu {
	
	public static String menu(String[] args) {
		java.awt.PopupMenu menu=new java.awt.PopupMenu();
		//	List<MenuItem> items= new ArrayList<MenuItem>();
		String[] options=args;
		MenuItem[] items=new MenuItem[options.length];
		for (int i = 0; i < options.length; i++) {
			items[i]=new MenuItem();
			items[i].setLabel(options[i]);
			menu.add(items[i]);
		}
		Component orig=new JFrame();
		orig.add(menu);
		
		Dimension d=new Dimension();
		d.height=100;
		d.width=200;
		orig.setMinimumSize(d);
		orig.show();
		orig.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				menu.show(orig,orig.getWidth()/2,orig.getHeight()/2);
			}
			@Override
			public void focusLost(FocusEvent e) {
			}
		});
		orig.addNotify();
		menu.show(orig,orig.getWidth()/2,orig.getHeight()/2);
		final String[] choosen = new String[1];
		ActionListener l=new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(e.getActionCommand());
			choosen[0] =e.getActionCommand();
			}
		};
		menu.addActionListener(l);
		return choosen[0];
		
		
	}
}
